<?php
require 'vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

function parsePostgresUrl($url)
{
    $parsed_url = parse_url($url);
    $query = [];
    if (isset($parsed_url['query'])) {
        parse_str($parsed_url['query'], $query);
    }
    return [
        'host' => $parsed_url['host'],
        'port' => $parsed_url['port'],
        'user' => $parsed_url['user'],
        'pass' => $parsed_url['pass'],
        'dbname' => ltrim($parsed_url['path'], '/'),
        'options' => $query
    ];
}

$parsed = parsePostgresUrl($_ENV['POSTGRES_URL']);

$host = $parsed['host'];
$port = $parsed['port'];
$db = $parsed['dbname'];
$user = $parsed['user'];
$pass = $parsed['pass'];
$options = $parsed['options'];

$dsn = "pgsql:host=$host;port=$port;dbname=$db";

if (isset($options['sslmode'])) {
    $dsn .= ";sslmode=" . $options['sslmode'];
}

try {
    $pdo = new PDO($dsn, $user, $pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit();
}