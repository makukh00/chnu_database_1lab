<?php
require 'db.php';

// Set content type to JSON
header('Content-Type: application/json');

// Get the table parameter
$table = $_GET['table'] ?? '';

// Validate the table parameter to prevent SQL injection
$validTables = ['machine_types', 'repair_types', 'repairs'];
if (!in_array($table, $validTables)) {
    echo json_encode([]);
    exit();
}

// Prepare the query based on the table parameter
$query = "SELECT * FROM {$table}";

try {
    // Execute the query
    $stmt = $pdo->query($query);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Output the data as JSON
    echo json_encode($data);
} catch (Exception $e) {
    // Handle any errors
    echo json_encode(['error' => $e->getMessage()]);
}