-- Create the database
CREATE DATABASE IF NOT EXISTS lab1sskbd;

USE lab1sskbd;

-- Create the machine_types table
CREATE TABLE machine_types
(
    machine_type_id     INT AUTO_INCREMENT PRIMARY KEY,
    country             VARCHAR(255) NOT NULL,
    year_of_manufacture YEAR         NOT NULL,
    brand               VARCHAR(255) NOT NULL
);

-- Create the repair_types table
CREATE TABLE repair_types
(
    repair_type_id   INT AUTO_INCREMENT PRIMARY KEY,
    name             VARCHAR(255)   NOT NULL,
    duration_in_days INT            NOT NULL,
    cost             DECIMAL(10, 2) NOT NULL,
    notes            TEXT
);

-- Create the repairs table
CREATE TABLE repairs
(
    repair_id       INT AUTO_INCREMENT PRIMARY KEY,
    machine_type_id INT  NOT NULL,
    repair_type_id  INT  NOT NULL,
    start_date      DATE NOT NULL,
    notes           TEXT,
    FOREIGN KEY (machine_type_id) REFERENCES machine_types (machine_type_id),
    FOREIGN KEY (repair_type_id) REFERENCES repair_types (repair_type_id)
);

-- Insert sample data

INSERT INTO machine_types (country, year_of_manufacture, brand)
VALUES ('Німеччина', 2015, 'БрендА'),
       ('США', 2018, 'БрендБ'),
       ('Японія', 2012, 'БрендВ'),
       ('Україна', 2020, 'БрендГ'),
       ('Італія', 2017, 'БрендД');

INSERT INTO repair_types (name, duration_in_days, cost, notes)
VALUES ('Плановий огляд', 2, 1500.00, 'Плановий технічний огляд обладнання.'),
       ('Заміна деталей', 5, 5000.00, 'Заміна зношених деталей.'),
       ('Повний ремонт', 10, 12000.00, 'Комплексний ремонт обладнання.'),
       ('Екстрений ремонт', 3, 8000.00, 'Екстрений ремонт через аварійний стан.'),
       ('Калібрування', 1, 2000.00, 'Калібрування і налаштування обладнання.'),
       ('Заміна мастила', 1, 1000.00, 'Заміна мастила в механізмах.'),
       ('Налаштування та тестування', 2, 2500.00, 'Налаштування та тестування після ремонту.'),
       ('Ремонт електроніки', 4, 7000.00, 'Ремонт електронних компонентів обладнання.'),
       ('Відновлення функціональності', 6, 9000.00, 'Відновлення працездатності обладнання.'),
       ('Оновлення програмного забезпечення', 2, 3000.00, 'Оновлення та налаштування програмного забезпечення.');

INSERT INTO repairs (machine_type_id, repair_type_id, start_date, notes)
VALUES (1, 1, '2024-01-15', 'Виконано плановий технічний огляд.'),
       (2, 2, '2024-02-20', 'Замінено зношені деталі.'),
       (3, 3, '2024-03-05', 'Здійснено комплексний ремонт обладнання.'),
       (4, 4, '2024-04-10', 'Проведено екстрений ремонт через аварійний стан.'),
       (5, 5, '2024-05-12', 'Калібрування та налаштування обладнання для підвищення точності.'),
       (1, 6, '2024-06-15', 'Замінено мастило в механізмах.'),
       (2, 7, '2024-07-20', 'Налаштування та тестування після ремонту.'),
       (3, 8, '2024-08-25', 'Ремонт електронних компонентів обладнання.'),
       (4, 9, '2024-09-30', 'Відновлення працездатності обладнання.'),
       (5, 10, '2024-10-05', 'Оновлення та налаштування програмного забезпечення.');

-- Create a new user 'admin' with all privileges
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'adminpassword';
GRANT ALL PRIVILEGES ON lab1sskbd.* TO 'admin'@'localhost';
FLUSH PRIVILEGES;
